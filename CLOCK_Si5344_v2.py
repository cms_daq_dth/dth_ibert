import sys
import hal
import os
import commands
import time
from I2CInterface import I2CInterface

# the first argumnet is the I2C address of the clock gen
# the second argument is the file to program

list_arg = sys.argv
first_card 	= 0
last_card 	= 100
single_card = 0
access 		= "read" # by default

next_arg		= 1
pos_arg_data	= 3
incr			= 0
line			= ""
address			= 0
page			= 0
reg_off			= 0
data			= 0
reg_data		= 0
command_i2c		= 0
end_file		= 0
page_mem		= 256
 
comp_add		= int(list_arg[1],0) 

#
# This code is searching for a DTH
#
try:
	dth = hal.PCIDevice("DTHAddressMap.dat", 0x10DC, 0x01B5, 0) 
except Exception:
		print "ERROR: Didn't find any DTH board!"
		exit(-1)
	
	
i2c_clock = I2CInterface(dth,comp_add, offsetWidth = 0x1 , dataWidth = 0x1, items={'i2c_a': 'CLOCK_i2c_access_a', 'i2c_b': 'CLOCK_i2c_access_b', 'i2c_poll': 'CLOCK_i2c_access_done'})

# dth.write("led_control",0x45,verifyFlag=True,offset=0)
# result = dth.read("debug_cst",0)
# print hex(int(result))

Check_device_ready = 0
#####################################
#	open  the file
file = open(list_arg[2], 'r')
 
while end_file != 1 :		# check end of file
	line = file.readline()	# read a line
	if line != '' :			# line is empty ? end of file
		if line[0] == "0" or line[0] == "P" or line[0] == "p" :	# line is not comment
		
			if line[0] == "P" or line[0] == "p" :  # execute a pause as request in the file
				time.sleep(0.1)
				print "Pause for calibration "	
				
			else :	
				address,data=line.split(",")
				page 		= int(address,0)/256
				reg_off 	= int(address,0)& 0xFF
				reg_data	= int(data,0) 
				 
				#####################################
				# execute command to I2C 
				if page != page_mem : 
					# change the page 
					i2c_clock.write(0x1,page)
 
					page_mem = page
				
				# write the register value	
 				i2c_clock.write(reg_off,reg_data)
 
	else :
		print "Done"
		end_file = 1




file.closed
