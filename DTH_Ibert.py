#!/usr/bin/python 
import sys
import os
import commands
import time
import hal 
from I2CInterface import I2CInterface
from consolemenu import *
from consolemenu.items import *
   
   
os.system('python enable_pci.py')
   
#####################################################
# This code is searching for a DTH
#
try:
	dth = hal.PCIDevice("DTHAddressMap.dat", 0x10DC, 0x01B5, 0)  
except Exception:
		print "ERROR: Didn't find any DTH board!"
		print "Can you reboot the COMExpress : sudo reboot"
		sys.exit(-1)
		
i2c_clk_switch 	= I2CInterface(dth,0xE0, offsetWidth = 0x1 , dataWidth = 0x1, items={'i2c_a': 'CLOCK_i2c_access_a', 'i2c_b': 'CLOCK_i2c_access_b', 'i2c_poll': 'CLOCK_i2c_access_done'})
		
i2c_FF_Switch 	= I2CInterface(dth,0XE0, offsetWidth = 0x1 , dataWidth = 0x1, items={'i2c_a': 'FireFly_i2c_access_a', 'i2c_b': 'FireFly_i2c_access_b', 'i2c_poll': 'FireFly_i2c_access_done'})
i2c_FF 			= I2CInterface(dth,0xA0, offsetWidth = 0x1 , dataWidth = 0x1, items={'i2c_a': 'FireFly_i2c_access_a', 'i2c_b': 'FireFly_i2c_access_b', 'i2c_poll': 'FireFly_i2c_access_done'})

#reset	I2C switch Firefly
dth.write("reset_hw_comp",0x80000000) 
time.sleep(1)
dth.write("reset_hw_comp",0x00000000)
	
comp_switch = [0x1,0x2,0x4,0x8,0x20,0x10] 



def Help():
	print "-------------------------------------------------------"
	print " HELP TO RUN THE IBERT on the 2 Fireflys of the DTH_p1"
	print "-------------------------------------------------------"
	
	print "-- you will have to reconfigure the reference clock  -- option 2) --"
	print "-- load the design in the FPGA, use -- option 3) -- to manage the PCIe configuration "
	print "-- Enable the FireFly -- option 4) -- "
	print "-- you can use the other options to enable disable CDR," 
	print "     change bitrate, monitor the power on received"
	
	done = raw_input("return")
	return
	
# configure le clock gen
def conf_clock():
	#   remove local reset
	reset_val	= dth.read("reset_hw_comp")
	#print "Reset val: " ,hex(reset_val)
	reset_val	= reset_val | 0x40000000
	#print "new reset :", hex(reset_val)
	dth.write("reset_hw_comp",reset_val,False,0)
	reset_val	= reset_val & 0xBFFFFFFF
	dth.write("reset_hw_comp",0,False,0)   

	# release clock reset
	dth.write("CLOCK_gen_status",0x00000111)
	time.sleep(1) 
	dth.write("CLOCK_gen_status",0x00000000) 
	
	bitrate_choice = raw_input("Which bitrate will you use 25.78125 Gb/s or 15.66 Gb/s  (Enter 25 or 15)")

	print "#########################################################################"
	print "--> Program clock reference  "
	print "-------------------------------------------------------------------------"
	# access the TCA9548 for clock control
	i2c_clk_switch.write(0x0,1)
	if 		bitrate_choice == "25" :
		os.system('python CLOCK_Si5344_v2.py 0xD0 IC20_DAQv2_322_125_125_125.txt' )
		print "Program ref clock 322.265625 MHz  for 25.78125 Gbs"
	elif bitrate_choice == "15" :
		os.system('python CLOCK_Si5344_v2.py 0xD0 IC20_DAQv2_1566_125_125_125.txt' )
		print "Program ref clock 156.6 MHz  for 15.66 Gbs"
	else :
		print "You didn't enter a correct choice! Try again"

	done = raw_input("return")
	return	
	
#load design in design
def reconf_FPGA():	
	
	Conf_val = ['0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'] 
	 
	####################################################################################################
	print "------read Configuration ------"
	#read 16 config words
	for x in range (0,16):
		offset = int(x) * 4
		Conf_val[x] 	= dth.read("ConfigStart",offset)
		print hex(Conf_val[x])
		
	print "You can load the bit and ltx files the FPGA with VIVADO"

	wait = raw_input("Hit ENTER when prog is DONE!")

	for x in range (0,16):
		offset = int(x) * 4
		dth.write("ConfigStart",Conf_val[x],False,offset) 
	 
	dth.write("Reset_glb",0x20000,False,offset)	
	
#enable Firefly
def Ena_FF():
 	# 
	reset_val	= dth.read("reset_hw_comp")
	#print "Reset val: " ,hex(reset_val)
	reset_val	= reset_val | 0x80000000
	#print "new reset :", hex(reset_val)
	dth.write("reset_hw_comp",reset_val,False,0)
	reset_val	= reset_val & 0x7FFFFFFF
	dth.write("reset_hw_comp",0,False,0)  
	
	FF_ena	= dth.read("Firefly_monitoring",0)
	print hex(FF_ena)
	dth.write("Firefly_monitoring",0x222222,False,0) 
	
	
	done = raw_input("return")
	return	
 
#CDR settings Firefly
def CDR_setting():
 	#   
	FF_ena	= dth.read("Firefly_monitoring",0)
	FF_present	= FF_ena & 0x444444
	print "Would you like to enable or disable "
	enable = raw_input("E or D ?")
	
	if enable == "E":
		CDR_ena	= 0xFF
	else : 
		CDR_ena	= 0x00
		
	for x in range(6): 
		if (FF_present & 0x4) == 0x0 :
			print "FF is there : ",x 
			i2c_FF_Switch.write(0x0,comp_switch[x])
			
			i2c_FF.write(98,CDR_ena)
			CDR = i2c_FF.read(98)
			print "CDR :", hex(CDR)
			
		FF_present = FF_present / 0x10
 
	done = raw_input("return")		
	return	
	 
#Read Rx poewr Firefly
def RxPower_FF():
 	#   	
	FF_ena	= dth.read("Firefly_monitoring",0)
	FF_present	= FF_ena & 0x444444
	
	for x in range(6): 
		if (FF_present & 0x4) == 0x0 :
			
			i2c_FF_Switch.write(0x0,comp_switch[x])
			print "****************************************"
			print "Firefly number : ",x
			for rx in range(0,4) :
				print "---------------------------------------------" 
				rx_power_high	= i2c_FF.read(34 + (rx * 2))
				print "Rx power MSB offset : " , 34 + (rx * 2), " -- val : ", hex(rx_power_high)
				rx_power_low	= i2c_FF.read(35 + (rx * 2))
				print "Rx power LSB offset : " , 35 + (rx * 2), " -- val : ",hex(rx_power_low)
				rx_power = (rx_power_high * 0x100 + rx_power_low ) * 0.100
				print ("RX power on link %d	 is"% rx, " %d uW: " % rx_power)	
					 
			
		FF_present = FF_present / 0x10 
	done = raw_input("return")		
	return

def Set_rate():
	FF_ena	= dth.read("Firefly_monitoring",0)
	FF_present	= FF_ena & 0x444444
	
	rate = raw_input("Which rate to be set ? 25Gb (0) or 28GB(1) ?")
	
	for x in range(6): 
		if (FF_present & 0x4) == 0x0 :
			if rate == '0':
				i2c_FF.write(99,0x00)
				print ("Rate  25 Gbs set")
		
			if rate == '1':
				i2c_FF.write(99,0xFF)
				print ("Rate  28 Gbs set")
	
	done = raw_input("return")
	return
	
#Disable tx on one Firefly
def Disable_tx_FF():
 	#   
	FF_ena	= dth.read("Firefly_monitoring",0)
	FF_present	= FF_ena & 0x444444
	
	print "**************************************************"
	print " ** Select J9 to configure TX on Slink Receiver **" 
	print "**************************************************"
	
	#  "J9"  Slink Receiver
	i2c_FF_Switch.write(0x0,0x1)
	#  "J12" Slkink Sender
	#	i2c_FF_Switch.write(0x0,0x8)
	
		
	print "*************************************************************"
	print " ** bit0 = TX1 * bit 1 = TX2 * bit 3 = TX3 * bit 4 = TX4  **" 
	print "*************************************************************"
	ff_sel = input("Enter the mask to disable TX '1' or enable TX '0' (ex : 0x1)?")
	print (ff_sel)
	i2c_FF.write(86,ff_sel) 
	
	#  "J12" Slkink Sender
	print " "
	print "Disable the same TX on the Slink Sender of the DTH (J12)"
	i2c_FF_Switch.write(0x0,0x8)
	i2c_FF.write(86,ff_sel)
	done = raw_input("return")		
	return		
	
 
def main():
    # Create the root menu
    menu = ConsoleMenu("DAQ FireFly control V3.0")
	# V3.0 implement the TX control on Firefly
	
	#Setup serdes
    function_item1 = FunctionItem("HELP", Help )
    function_item2 = FunctionItem("Configure reference clock", conf_clock )
    function_item3 = FunctionItem("Load the design in the FPGA", reconf_FPGA )
    function_item4 = FunctionItem("Enable FireFly", Ena_FF )
    function_item5 = FunctionItem("CDR setting", CDR_setting ) 
    function_item6 = FunctionItem("Read Rx power ", RxPower_FF )
    function_item7 = FunctionItem("Set 25Gb or 28 Gbs ", Set_rate ) 
    function_item8 = FunctionItem("Disable TX laser ", Disable_tx_FF ) 
	
 
    # Add all the items to the root menu 
	
    menu.append_item(function_item1)
    menu.append_item(function_item2)
    menu.append_item(function_item3)
    menu.append_item(function_item4)
    menu.append_item(function_item5)
    menu.append_item(function_item6)
    menu.append_item(function_item7) 
    menu.append_item(function_item8) 
 
    # Show the menu
    menu.start()
    menu.join()


if __name__ == "__main__":
    main() 

	
