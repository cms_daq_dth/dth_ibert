
class I2CInterface :
    """A class to facilitate I2C accesses with pyHAL.

    The class provides a generic interface to perform i2c read and write accesses via an FPGA.
    Each i2c device is handled by an instance of this class."""
    
    def __init__(self, pcidev,
                 deviceAdr,
                 offsetWidth,
                 dataWidth,
                 debugFlag = False,
                 items = { 'i2c_a'    : "CLOCK_i2c_access_a",
                           'i2c_b'    : "CLOCK_i2c_access_b",
                           'i2c_poll' : "CLOCK_i2c_access_done" } ):
        """The constructor needs essential parameters of the i2c device.

        Args:
            pcidev: An instance of the PCIDevice from the PyHAL.
            deviceAdr: The I2C address of the device. Allowed range : 0...255 (1 byte)
            offsetWidth: The width (in bytes) of the register offset. Allowed range 0...2.
            dataWidth: The width (in bytes) of the data to be transfered. Allowed range 1...4.
            debugFlag: If True some debug messages are printed to the console (default False).
            items: A dictionary which must contains three PyHAL address items which are used to 
                   perform the I2C access. The following entries MUST be provided:
                   i2c_a is the item used to set the device address, offset- and data-width
                         and the register offset.
                   i2c_b is the item used to set the data or to read the data.
                   i2c__poll must have a bit mask of two neigbouring bits which are polled to 
                             establish if and access was successful or not. If one of the bits 
                             goes high the access is considrered to be complete. If Bit 1 went 
                             high an error occured and a RuntimeError is raised. If Bit 0 went 
                             high the access is considered successfull. A timeout of 1sec is 
                             implemented. A runtime error is raised if after this time none of 
                             the bits went high.
        """

        self.pcidev      = pcidev
        if ( deviceAdr < 0 or deviceAdr > 255 ):
            raise RuntimeError( "I2CInterface constructor : the deviceAdr must be in the range 0..255 but is given as "
                                + str(deviceAdr) )

        self.deviceAdr   = deviceAdr
        if ( offsetWidth < 0 or offsetWidth > 2 ) :
            raise RuntimeError( "I2CInterface constructor : the offsetWidth must be in the range 0..2 but is given as "
                                + str(deviceAdr) )
            
        self.offsetWidth = offsetWidth
        if ( dataWidth < 1 or dataWidth > 4 ) :
            raise RuntimeError( "I2CInterface constructor : the dataWidth must be in the range 1..4 but is given as "
                                + str(deviceAdr) )
        self.dataWidth   = dataWidth
        self.debugFlag   = debugFlag
        self.i2cmask     = self.deviceAdr * 0x1000000 + self.offsetWidth * 0x100000 + self.dataWidth * 0x20000
        self.i2c_a    = items['i2c_a']
        self.i2c_b    = items['i2c_b']
        self.i2c_poll = items['i2c_poll']
            

        self.debug( "Created Interface : deviceAdr " + str(deviceAdr) +
                    " offsetWidth " + repr(offsetWidth) +
                    " dataWidth " + repr(dataWidth) +
                    " debugFlag " + repr(debugFlag) )
    
    def debug( self, str ):
        '''Prints out some debugging info if the debugFlag in the constructor is set True.'''
        if ( self.debugFlag ):
            print( str )
    
         

    def write( self, register, data ):
        '''Write the specified data to the specified register'''
        
        self.debug( "Write to register " + hex(register) + " value " + hex(data) )
        
        if ( register < 0 or 
             (self.offsetWidth > 0 and (register > (1 << (8 * self.offsetWidth))-1 )) or 
             (self.offsetWidth == 0 and register != 0)) :
            raise RuntimeError( "I2CInterface.write : The register value is out of the allowed range : " +
                                str(register) + " does not fit in ", str(self.offsetWidth) + " bytes" )
        
        if ( data < 0 or data > (1 << (8 * self.dataWidth))-1 ) :
            raise RuntimeError( "I2CInterface.write : The data value is out of the allowed range : " +
                                str(data) + " does not fit in ", repr(self.dataWidth) + " bytes" )
        
        # write the register value
        
        self.debug( "write: " + self.i2c_b + " " + hex(data))
        self.pcidev.write(    self.i2c_b, data )
        self.debug( "write: " + self.i2c_a + " " + hex(self.i2cmask+register))
        self.pcidev.write(    self.i2c_a, self.i2cmask + register )
        self.debug("now polling")
        pollResult = self.pcidev.pollItem( self.i2c_poll,0, 1, HAL_POLL_UNTIL_DIFFERENT )
        self.debug( "pollResult " + repr(pollResult))
        if pollResult & 0x2 :
            raise RuntimeError( "I2CInterface.write : Error while writing to register {:#x} (dec:{:d}) data {:#x}".format( register, register, data ) )

    def read( self, register, noStop = False ):
        '''Read the specified register. If the noStop flag is set the FPGA will not issue
           a Stop seqence between the setting of the address and the reading of the data. 
           Some chips require to not have this stop sequence. However the default is to 
           have the Stop sequence present.'''

        self.debug( "Read from register " + hex(register) )

        if ( register < 0 or 
             (self.offsetWidth > 0 and (register > (1 << (8 * self.offsetWidth)-1) )) or 
             (self.offsetWidth == 0 and register != 0)) :
        #if ( register < 0 or register > (1 << (8 * self.offsetWidth)-1) ) :
            raise RuntimeError( "I2CInterface.read : The register value is out of the allowed range : " +
                                str(register) + " does not fit in ", repr(self.offsetWidth) + " bytes" )
        

        # read the register value
        self.pcidev.write( self.i2c_a, self.i2cmask + 0x10000 * int(noStop) + register + 0x1000000 )
        pollResult = self.pcidev.pollItem(    self.i2c_poll,0, 1, HAL_POLL_UNTIL_DIFFERENT )
        if pollResult & 0x2 :
            raise RuntimeError( "I2CInterface.read : Error while reading from register {:#x} (dec:{:d})".format( register, register ) )

        readValue = self.pcidev.read( self.i2c_b )

        mask = (1 << self.dataWidth*8) - 1
        return (readValue & mask)


################ some dummy declarations for testing without hardware #################
class PCI:

    def write( self, it, val ):
        print ( "Writing    : {:25s} 0x{:08x}".format( it, val ) )

    def read( self, it ):
        print ( "Reading    : {:25s}".format( it ) )

    def pollItem( self, it, val, time, pollMethog = 0 ):
        print ( "Polling    : {:25s}".format( it ) )
        return 0x10000
        
    def read( self, it ):
        print ( "Reading    : {:25s}".format( it ) )

HAL_POLL_UNTIL_DIFFERENT = 1


if __name__ == "__main__" :

    pci = PCI()

    i2c = I2CInterface( pci, 0x03, offsetWidth = 0x1, dataWidth = 0x4, debugFlag = True )

    i2c.write( 05, 0xabcd )
