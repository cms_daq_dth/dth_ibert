import sys
import os
import commands
import time
import hal 

#
# This code is searching for a DTH
#
try:
	dth= hal.PCIDevice("DTHAddressMap.dat", 0xECD6, 0xFEA1, 0) 
	Card_dth = 1
	print "DTH-p1-v1"
except Exception:
	try :
		dth = hal.PCIDevice("DTHAddressMap.dat", 0x10DC, 0x01B5, 0)
		Card_dth = 2		
		print "DTH-p1-v2"
	except Exception:
		print "ERROR: Didn't find any DTH board!"
		exit(-1)
	
 
####################################################################################################

PCIe_enabled = dth.read("enable_Memory_Space",0x0)

if (PCIe_enabled & 0x1) == 0 :
	dth.write("enable_Memory_Space",0x1,False,0x0) 
	print "------We had to enable PCIe memory access !------"
   
 
