##########################################
#  D. Gigi
#  Ibert on DTH P1 v1.0
#
#  Nov. 2020
#  CERN EP/CMD
#########################################
#
The directory contains software and firmware to run Ibert on the 2 Fireflys installed on the DTH-P1.
For 25.78125Gb/s and 15.66Gb/s

The bit and ltx files should be used in VIVADO to load the Design in the DTH (only in the FPGA, do not program this in the flash)

Usage:
------

Execute the following steps in the given sequence to use the debugging firmware:

1) Go into the directory of the cloned repository (where the DTH_Ibert.py file is)
2) Launch the DTH_Ibert.py programme
3) Reprogram the clocks on the board
4) Launch the option to re-programme the FPGA (this saves the PCI config space for later)
5) Programme the FPGA with the Xilinx programmer (DIP connector at the DTH fron panel)
6) Press 'Enter' to restore the PCI configuration space (which was stored under point 4)
7) Use the firmware with the rest of the menu points and debug your links with Ibert
8) To go back to the standard configuation power-cycle the DTH to load default firmware and clock configuration.
